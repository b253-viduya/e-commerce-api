const bcrypt = require("bcrypt");
const User = require("../models/User");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    });

    return newUser.save()
        .then(user => user ? true : false)
        .catch(err => err);
};


module.exports.loginUser = (reqBody) => {
    return User.findOne({
            email: reqBody.email
        })
        .then(result => {
            if (!result) return false;
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            return isPasswordCorrect ? {
                access: auth.createAccessToken(result)
            } : false;
        })
        .catch(err => err);
};