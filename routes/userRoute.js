const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");


router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


module.exports = router;