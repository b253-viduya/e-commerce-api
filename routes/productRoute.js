const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

// Create product (admin only)
router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (!userData.isAdmin) return res.send(false);

    productController.addProduct(req.body)
        .then(result => res.send(result))
        .catch(err => res.send(err));
});

// Routes for retrieving all Products
router.get("/all", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (!userData.isAdmin) return res.send(false);

    productController.getAllProducts()
        .then(result => res.send(result))
        .catch(err => res.send(err));
});

// Routes for retrieving all Active Products
router.get("/", (req, res) => {
    productController.getAllActive()
        .then(result => res.send(result))
        .catch(err => res.send(err));
});

// Retrieve single product
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Update Product information (Admin only)
router.put("/:productId", auth.verify, (req, res) => {
    if (!auth.decode(req.headers.authorization).isAdmin) return res.send(false);

    productController.updateProduct(req.params, req.body)
        .then(result => res.send(result))
        .catch(err => res.send(err));
});


// Archieve Product (admin only)
router.patch("/:productId/archive", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if (!userData.isAdmin) return res.send(false);

    productController.archiveProduct(req.params)
        .then(result => res.send(result))
        .catch(err => res.send(err));
});


module.exports = router;