const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
// const orderRoutes = require("./routes/orderRoute");

const app = express();
const port = process.env.PORT || 4000
const db = mongoose.connection


// Connect to MOngoDB database
mongoose.connect("mongodb+srv://admin:admin123@batch253-jeroxzi.6um7zso.mongodb.net/s41-46?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
db.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use("/users", userRoute);
app.use("/products", productRoute);
// app.use("/orders", orderRoute);

if (require.main === module) {
    app.listen(port, () => {
        console.log(`API is now online on port ${ port }`)
    });
}

module.exports = app;